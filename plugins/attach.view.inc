<?php
/**
 * Renders attached view.
 */
function attach_view_process($params) {
  $name = $params['name'];
  $display = $params['display'];
  $args = $params['args'];
  $args = explode('/', $args);
  $view = call_user_func_array('views_embed_view', array_merge(array($name, $display), $args));
  return render($view);
}
